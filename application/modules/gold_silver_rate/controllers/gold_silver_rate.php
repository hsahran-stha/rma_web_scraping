<?php
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/libraries/Format.php');

use chriskacerguis\RestServer\RestController;

class Gold_Silver_rate extends RestController
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_get()
	{
		$url = WEB_URL;
		$html = file_get_html($url);
		$data = $html->find(".gold-silver-rate .gold-silver-table");

		$result = array();
		if (isset($data[0])) {
			foreach ($data[0]->children() as $key => $row) {
				if ($key !== 0) {
					$result[$key]["particulars"] = preg_replace('/\s+/', '', $row->children(0)->text());
					$result[$key]["units"] = preg_replace('/\s+/', '', $row->children(1)->text());
					$result[$key]["rate"] = preg_replace('/\s+/', '', $row->children(2)->text());
				}
			}
			return $this->response($result, 200);
		}
	}
}
