<?php
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/libraries/Format.php');

use chriskacerguis\RestServer\RestController;

class Exchange_Rate extends RestController
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_get()
	{
		$url = WEB_URL;
		$html = file_get_html($url);
		$data = $html->find(".exchange-rate .exchange-rate-table");

		$result = array();
		if (isset($data[0])) {
			foreach ($data[0]->children() as $key => $row) {
				if ($key !== 0) {
					$result[$key]["currency"] = explode('--> ', $row->children(0)->innertext())[1];
					$result[$key]["note"]["buy"] = preg_replace('/\s+/', '', $row->children(1)->text());
					$result[$key]["note"]["sell"] = preg_replace('/\s+/', '', $row->children(2)->text());
					$result[$key]["tt"]["buy"] = preg_replace('/\s+/', '', $row->children(3)->text());
					$result[$key]["tt"]["sell"] = preg_replace('/\s+/', '', $row->children(4)->text());
				}
			}
			return $this->response($result, 200);
		}
	}
}
